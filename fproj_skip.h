// Copyright (c) 2007 Lionel Moisan <Lionel.Moisan@parisdescartes.fr>

#include "library_skip.h"
#include <vector>


//void fproj(float *in, float *out, int nx, int ny, int *sx, int *sy, float *bg, int *o, float *p, char *i, float X1, float Y1, float X2, float Y2, float X3, float Y3, float *x4, float *y4);
void fproj(std::vector<float>& in, std::vector<float>& out, int nx, int ny, int *sx, int *sy, float *bg, int *o, float *p, char *i, float X1, float Y1, float X2, float Y2, float X3, float Y3, float *x4, float *y4);
void fproj(std::vector<float>& in, std::vector<float>& out, int nx, int ny, int *sx, int *sy, float *bg, int *o, float *p, char *i, float X1, float Y1, float X2, float Y2, float X3, float Y3, float *x4, float *y4, bool** flag_skip_sift_rot, bool** &flag_skip_sift_proj);

