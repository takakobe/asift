#pragma once
#include "opencv2/opencv.hpp"
class ASIFTFeatureDetectorAndDescriptorExtractor
{
public:
	ASIFTFeatureDetectorAndDescriptorExtractor();
	~ASIFTFeatureDetectorAndDescriptorExtractor();

	void compute(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors, int affine_trans_level = 1) const;
};

