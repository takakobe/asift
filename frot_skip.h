// Copyright (c) 2007 Lionel Moisan <Lionel.Moisan@parisdescartes.fr>

#include "library_skip.h"
#include <vector>



/*void frot(float *in, float *out, int nx, int ny, int *nx_out, int *ny_out, float *a, float *b, char *k_flag)*/
//void frot(float *, float (*)[], int, int, int *, int *, float *, float *, char *);
void frot(std::vector<float>&, std::vector<float>&, int, int, int *, int *, float *, float *, char *);
void frot(std::vector<float>&, std::vector<float>&, int, int, int *, int *, float *, float *, char *, bool**, bool**&);
