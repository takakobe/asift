#if defined(_AFX) || defined(_AFXDLL)
#include "stdafx.h"
#endif
 
#include "ASIFT.h"

#include "demo_lib_sift_skip.h"
//#include "io_png/io_png.h"
#include "library_skip.h"
#include "frot_skip.h"
#include "fproj_skip.h"
#include "compute_asift_keypoints_skip.h"

ASIFTFeatureDetectorAndDescriptorExtractor::ASIFTFeatureDetectorAndDescriptorExtractor()
{
}


ASIFTFeatureDetectorAndDescriptorExtractor::~ASIFTFeatureDetectorAndDescriptorExtractor()
{
}


void ASIFTFeatureDetectorAndDescriptorExtractor::
compute(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors, int affine_trans_level) const
{
	IplImage img = IplImage(image);
	int width = img.width;
	int height = img.height;

	//ASIFTはpng形式の画像しか読み込めないのでIplImage→pngに変換
	std::vector<float> ipixels1;
	ipixels1.resize(img.width * img.height);
	uchar b, g, r;
	/*for(int  y = 0; y < img.rows; y++)
	{
	for(int x = 0; x < img.cols; x++)
	{
	b = (uchar*)(img.data[y*img.step + x*img.elemSize + 0]);
	g = (uchar*)(img.data[y*img.step + x*img.elemSize + 1]);
	r = (uchar*)(img.data[y*img.step + x*img.elemSize + 2]);
	ipixels1[y*img.cols + x] = (6969 * r + 23434 * g + 2365 * b) / 32768;
	}
	}*/

	for (int j = 0; j<img.height; j++)
	{
		for (int i = 0; i<img.width; i++)
		{
			b = ((uchar*)(img.imageData + img.widthStep * j))[i * 3];
			g = ((uchar*)(img.imageData + img.widthStep * j))[i * 3 + 1];
			r = ((uchar*)(img.imageData + img.widthStep * j))[i * 3 + 2];
			ipixels1[j*img.width + i] = (float)(6969 * r + 23434 * g + 2365 * b) / 32768;
			//cout << ipixels1[j*img->width + i] << " ";
			//cout << (float)img->imageData[img->widthStep*j+i] << endl;
		}
	}

	//dim = 128; type = CV_8U;
	//std::vector<float> ipixels1;
	//int width = image.cols;
	//int height = image.rows;
	//if (image.channels() == 3)
	//{
	//	// convert mat to png

	//	ipixels1.resize(width * height);

	//	for (int y = 0; y < height; y++)
	//	{
	//		for (int x = 0; x < width; x++)
	//		{
	//			const cv::Vec3b &p = image.at<cv::Vec3b>(y, x);
	//			ipixels1[y*height + x] = static_cast<float>(6969 * p[2] + 23434 * p[1] + 2365 * p[0]) / 32768.F;
	//		}
	//	}
	//}
	//else if (image.channels() == 1)
	//{
	//	for (int y = 0; y < height; y++)
	//	{
	//		for (int x = 0; x < width; x++)
	//		{
	//			const uchar &p = image.at<uchar>(y, x);
	//			ipixels1[y*height + x] = static_cast<float>(p);
	//		}
	//	}
	//}

	// SIFTをスキップするためのフラグの初期化
	bool **flag_skip_sift;
	flag_skip_sift = new bool*[width];
	for (int i = 0; i < width; i++)
		flag_skip_sift[i] = new bool[height];
	for (int i = 0; i < width; i++)
	for (int j = 0; j < height; j++)
		flag_skip_sift[i][j] = false;

	int verb = 0;//処理の要所要所でテキストを出力するか

	///// Compute ASIFT keypoints
	// number N of tilts to simulate t = 1, \sqrt{2}, (\sqrt{2})^2, ..., {\sqrt{2}}^(N-1)
	//int num_of_tilts1 = sqrt(2.0); //default = 7

	// Define the SIFT parameters
	siftPar siftparameters;
	default_sift_parameters(siftparameters);

	std::vector< std::vector<keypointslist> > keys1;
	int query_length = (unsigned int)compute_asift_keypoints(ipixels1, width, height, affine_trans_level, verb, keys1, siftparameters, flag_skip_sift);

	int fnum = 0;
	for (int i = 0; i < (int)keys1.size(); i++)
		for (int j = 0; j < (int)keys1[i].size(); j++)
			fnum += (int)keys1[i][j].size();
		keypoints.reserve(fnum);
		descriptors = cv::Mat(fnum, 128, CV_32F);

		int count = 0;
		for (int i = 0; i < (int)keys1.size(); i++){
			for (int j = 0; j < (int)keys1[i].size(); j++){
				for (int k = 0; k < (int)keys1[i][j].size(); k++)
				{
					keypoints.push_back(cv::KeyPoint());
					keypoints.back().pt.x = keys1[i][j][k].x;
					keypoints.back().pt.y = keys1[i][j][k].y;
					keypoints.back().size = keys1[i][j][k].scale;
					keypoints.back().angle = keys1[i][j][k].angle;
					for (int ii = 0; ii < 128; ii++)
						descriptors.ptr<float>(count)[ii] = static_cast<uchar>(keys1[i][j][k].vec[ii]);
					++count;
				}
			}
		}

	for (int i = 0; i < width; i++)
		delete[] flag_skip_sift[i];
	delete[] flag_skip_sift;
}